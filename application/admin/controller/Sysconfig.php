<?php
namespace app\admin\controller;
use app\admin\common\Auth;
use app\admin\common\Tree;
use think\Controller;
use think\Db;
use think\Session;

class Sysconfig extends Base {
    public function index() {
        return "safdasdf";
    }

    public function siteConfig() {
        if ($this->request->isPost()) {
            $params = $this->request->param();
            Db::startTrans();
            try {
                foreach ($params as $key => $value) {
                    Db::table("think_site_config")->where("name", $key)->update(["value" => $value]);
                }
                Db::commit();
            } catch (\Exception $e) {
                Db::rollback();
                $this->error("更新失败！", "siteConfig", '', 3);
            }

            $this->success("更新成功！", "siteConfig");

        } else {
            $res = Db::table('think_site_config')->column("value", "name");

            $this->assign("data", $res);
            return $this->fetch();
        }

    }

    public function mailConfig() {
        return $this->fetch();
    }
    public function smsConfig() {
        return $this->fetch();
    }
}
?>


