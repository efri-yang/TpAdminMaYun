<?php
namespace app\index\controller;
use app\index\common\Tree;
use think\Controller;
use think\Db;

class Index extends Base {
    public function index() {

        //获取banner图片

        $selRes = Db::table("think_article")->field('id,carouselimg')->where("carouselimg", "not null")->select();

        if (count($selRes) > 4) {
            array_splice($selRes, 4);
        }
        $this->assign("bannerList", $selRes);

        //获取产品分类
        $proSel = Db::table("think_category")->where("sign", "production")->select();

        $this->assign("proList", $proSel);

        return $this->fetch();

    }
}
