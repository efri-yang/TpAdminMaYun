<?php
namespace app\index\controller;

use think\Controller;
use think\Db;
use think\Request;

class Article extends Controller {
    public function index() {

    }

    public function detail() {
        $request = Request::instance();
        $params = $request->param();
        $data = Db::table("think_article")->where("id", $params["id"])->find();
        $data["tagid"] = explode(",", $data["tagid"]);
        $data["tags"] = explode(",", $data["tags"]);

        $data["content"] = stripslashes($data["content"]);
        $this->assign([
            "article" => $data,
        ]);
        return $this->fetch();
    }
}
