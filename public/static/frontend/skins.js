var skins = [
	{
        "name": "united",
        "bg": "#dd4814",
        "color": "#ffffff",
        "content": "默认模板 united",
		"css": "css/united.skin.css"
    },
    {
        "name": "cerulean",
        "bg": "#317eac",
        "color": "#ffffff",
        "content": "cerulean",
        "css": "css/cerulean.skin.css"
    },
	{
        "name": "slate",
        "bg": "#484e55",
        "color": "#ffffff",
        "content": "slate",
        "css": "css/slate.skin.css"
    },
]