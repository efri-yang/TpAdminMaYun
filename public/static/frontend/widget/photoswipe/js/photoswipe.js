// JavaScript Document
(function($){	
	$.fn.photoSwipe = function (options) {
		if(this.length == 0) return this;
        
		if(this.length > 1){
			this.each(function(){$(this).photoSwipe(options)});
			return this;
		}
		var defaults={
			
		}
		var opts=$.extend(defaults,options);
		var $this=$(this),
			$divbox=$this.children('div'),
			Length=$divbox.length,
			$photoWrap,
			$em,
			$pic,
			$exit,
			$play,
			$prev,
			$next,
			photoW,
			picI=0,
			photoTime=false,
			picTimer,
			_photoInit=function(index){
					picI=index;
					if($photoWrap){
						$photoWrap.show();
						$em.text(picI+1)
					}else{
						$photoWrap=$('<div class="photo-swipe-wrap" />').appendTo('body');
						$('<div class="photo-swipe-number"><span>/'+Length+'</span></div>').appendTo($photoWrap);
						$em=$('<em>'+(picI+1)+'</em>').prependTo($photoWrap.find('.photo-swipe-number>span'));
						$pic=$('<div class="photo-swipe-pic" />').appendTo($photoWrap);
						var photoHtml='';
						for (var i=0; i<Length; i++){
							photoHtml+='<div><p><img src="'+$this.children('div').eq(i).find('a.thumbnail').attr('href')+'" /></p></div>';
						}
						$pic.html(photoHtml);
						$('<div class="photo-swipe-btn" />').appendTo($photoWrap);
						$exit=$('<span class="exit"><i></i></span>').appendTo($photoWrap.find('.photo-swipe-btn')).bind('click',function(){
								$photoWrap.hide()
								if(picTimer){clearTimeout(picTimer);}
							});
						$play=$('<span class="play"><i></i></span>').appendTo($photoWrap.find('.photo-swipe-btn')).bind('click',function(){
								if(photoTime){
									photoTime=false;
									$(this).removeClass('on');
									clearTimeout(picTimer);
								}else{
									photoTime=true;
									$(this).addClass('on');
									picTimer=setTimeout(_photoAutoSlider,3000);	
								}
							});
						$prev=$('<span class="prev"><i></i></span>').appendTo($photoWrap.find('.photo-swipe-btn')).bind('click',function(){
								if(picI-1>-1){
									picI--
									_photoSlider();
								}
							});
						$next=$('<span class="next"><i></i></span>').appendTo($photoWrap.find('.photo-swipe-btn')).bind('click',function(){
								if(picI+1<$pic.children('div').length){
									picI++
									_photoSlider();
								}
							});
						_photoResize();						
					}
					_photoSlider();
				},
				_photoResize=function(){
					if($photoWrap){
						var w=$(window).width(), h=$(window).height(),minH=h-70;
						photoW=w;
						$pic.height(minH).width(Length*w);
						$pic.children('div').each(function(index){
							var CSS = {
								'height'            : minH,
								'width'             : w
							}
							$(this).css(CSS).find('p').width(w).height(minH)
						})
						$pic.animate({'left':'-'+photoW*picI+'px'},500);						
					}
				},
				_photoSlider=function(){
					$pic.animate({'left':'-'+photoW*picI+'px'},500)
					$em.text(picI+1)
				},
				_photoAutoSlider=function(){
					if(picI>=Length-1){
						picI=0	
					}else{
						picI++	
					}
					_photoSlider();	
					picTimer=setTimeout(_photoAutoSlider,3000);	
				},
				_Throttle = function(fn, delay, mustRunDelay){
					var timer = null;
					var t_start;
					return function(){
						var context = this, args = arguments, t_curr = +new Date();
						clearTimeout(timer);
						if(!t_start){
							t_start = t_curr;
						}
						if(t_curr - t_start >= mustRunDelay){
							fn.apply(context, args);
							t_start = t_curr;
						}
						else {
							timer = setTimeout(function(){
								fn.apply(context, args);
							}, delay);
						}
					};
				}
			$divbox.find('a').on('click',function(){
				var index=$(this).parent().index();
				_photoInit(index);
				return false;
			});
			$(window).resize(function(){_Throttle(_photoResize(),50,30)})		
	}
	
})(jQuery); 