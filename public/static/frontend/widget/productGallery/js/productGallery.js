;(function($){
   var defaults={
		 thumbnailSmall:".product-thumbnail-small",
		 thumbnailPrev:".product-thumbnail-prev",
		 thumbnailNext:".product-thumbnail-next",
		 thumbnailPrevDis:"product-thumbnail-prev-dis",
		 thumbnailNextDis:"product-thumbnail-next-dis",
		 moveWidth:90,
		 currentIndex: 0
   }
   
   
  function arrowClassHandler(opts,len,prevArrow,nextArrow){
	  if(opts.currentIndex==0){
		prevArrow.addClass(opts.thumbnailPrevDis); 
	  }else if(opts.currentIndex==len-1){
		nextArrow.addClass(opts.thumbnailNextDis); 
	  }else{
		  prevArrow.removeClass(opts.thumbnailPrevDis);
		  nextArrow.removeClass(opts.thumbnailNextDis)
	   }
  }
 
	    
  $.fn.productGallery=function(options){
	    if(this.length !=1) return;//如果只有一张的时候,那么就不需要任何的事件了
		var opts=$.extend(defaults,options),
		    $thumbnailSmall=this.find(opts.thumbnailSmall).eq(0),
			$thumbnailPrev=this.find(opts.thumbnailPrev).eq(0),
			$thumbnailA=$thumbnailSmall.find("a"),
			$thumbnailNext=this.find(opts.thumbnailNext).eq(0),
			$thumbnailUL=$thumbnailSmall.children("ul").eq(0),
			$bigLi=this.children().children("ul").children("li"),
			len=$thumbnailA.length,viewLen;
	    var thumbnailSmallW=parseInt($thumbnailSmall.css("width")),
		    thumbnailUlW=parseInt($thumbnailUL.css("width"));
			
			
	   $(window).on("resize",function(){
		   thumbnailSmallW=parseInt($thumbnailSmall.css("width"));
		  
	  })
			
	   /* var $bigwrap=$('<div class="product-gallery-bigbox" id="J_product-gallery-bigbox"></div>'),
		    $bigul=$('<ul id="J_product-gallery-biglist" class="clearfix product-gallery-biglist"></ul>'),
			$bigli=[];
			
			$bigul.appendTo($bigwrap);
			$bigwrap.prependTo(this);*/
			
		var init=function(){
			arrowClassHandler(opts,$thumbnailPrev,$thumbnailNext);
			$thumbnailUL.css("width",opts.moveWidth*len);
			$thumbnailA.eq(0).addClass("oncurr");
			/*$thumbnailA.each(function(index,element){
				var $middlesrc=$(element).attr("data-middle"), $bigsrc=$(element).attr("data-big");
				if(opts.currentIndex==index){
					$bigli.push('<li><a href="'+$bigsrc+'" class="thumbnail"><img src="'+$middlesrc+'" /></a></li>');
				}else{
					$bigli.push('<li class="undis"><a href="'+$bigsrc+'" class="thumbnail"><img src="'+$middlesrc+'" /></a></li>');
				}
				
			})
			
			$bigul.append($bigli.join(''));
			$bigwrap.append($bigul);
			$bigwrap.appendTo($(this));*/
			
			
		}
	    
	    $thumbnailPrev.on("click",function(){
			 var thumbnailUlLeft=Math.abs(parseInt($thumbnailUL.css("left")));
			 if(thumbnailUlLeft==0){
			    return false;	 
			 }else{
				
					$thumbnailUL.animate({"left":"+="+opts.moveWidth},250,function(){
					  arrowClassHandler(opts,len,$thumbnailPrev,$thumbnailNext);
					   return false;   	
				})
				
			 } 		
	    })
		
		$thumbnailNext.on("click",function(){
			   var thumbnailUlLeft=Math.abs(parseInt($thumbnailUL.css("left")));
			   if(thumbnailSmallW > thumbnailUlW){
				 return false;   
			   }else{
				   if((thumbnailUlW-thumbnailUlLeft) > thumbnailSmallW){
					   $thumbnailUL.animate({"left":"+=-"+opts.moveWidth},250,function(){
					        arrowClassHandler(opts,len,$thumbnailPrev,$thumbnailNext);	
				       })
					  return false;   
				   }else{
					  return false;   
				   }
			   }
		    	
	    })
		$thumbnailA.each(function(){
			 $(this).on("click",function(){
				 var index=$thumbnailA.index(this);
				 $thumbnailA.removeClass("oncurr").eq(index).addClass("oncurr");
				 $bigLi.eq(index).fadeIn().siblings().hide();
				 return false;
			 })
	    })
		
		
		init()
		 
  }	
 
})(jQuery)
