<?php

require_once "PluploadHandler.php";

$base_path = substr($_SERVER['SCRIPT_NAME'], 0, strpos($_SERVER['SCRIPT_NAME'], '/TpAdminMaYun'));
$path = $_SERVER['DOCUMENT_ROOT'] . (substr($base_path, 0, 1) == "/" ? "" : "/") . "uploads" . DIRECTORY_SEPARATOR . date("Ymd");

$ph = new PluploadHandler(array(
    'target_dir' => $path,
    // 'target_dir' => "../uploads/ss",
    'allow_extensions' => 'jpg,jpeg,png,zip',
));

$ph->sendNoCacheHeaders();
$ph->sendCORSHeaders();

if ($result = $ph->handleUpload()) {
    $result["url"] = (substr($base_path, 0, 1) == "/" ? "" : "/") . "uploads" . DIRECTORY_SEPARATOR . date("Ymd") . DIRECTORY_SEPARATOR . $result["name"];
    die(json_encode(array(
        'OK' => 1,
        'info' => $result,
    )));
} else {
    die(json_encode(array(
        'OK' => 0,
        'error' => array(
            'code' => $ph->getErrorCode(),
            'message' => $ph->getErrorMessage(),
        ),
    )));
}